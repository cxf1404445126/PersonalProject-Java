package wCount;

import wCount.FileDeal;
import wCount.WordDeal;

import java.io.IOException;
import java.util.*;

public class Main {

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		String file = sc.next();
		FileDeal fd = new FileDeal();
		String[] wFreq;
		List sortMap;

		String text = fd.FileToString(file);
		WordDeal wd = new WordDeal(text);
		// 调用类中的方法获取相应的数值
		int charNum = wd.getCharCount();
		int wordCount = wd.getWordCount();
		int ValidLine = wd.getLineCount();
		Map<String, Integer> wordFreq = wd.getWordFreq();
		sortMap = wd.sortMap(wordFreq);
		wFreq = wd.ListToArray(sortMap);
		String w = charNum + "\r\n" + wordCount + "\r\n" + ValidLine + "\r\n";
		for (int i = 0; i < wFreq.length; i++) {
			w = w + wFreq[i] + "\r\n";
		}
		System.out.println(w);
		fd.WriteToFile(w);

	}

}
