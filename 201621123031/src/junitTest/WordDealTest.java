package junitTest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import wCount.FileDeal;
import wCount.WordDeal;

public class WordDealTest {

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testGetCharCount() throws IOException {//统计字符数量测试
		FileDeal fd = new FileDeal();
		String text1 = fd.FileToString("text/text1.txt");	
		String text2 = fd.FileToString("text/text2.txt");	
		String text3 = fd.FileToString("text/text3.txt");
		String text4 = fd.FileToString("text/text4.txt");
	
		
		WordDeal wd1 = new WordDeal(text1);
		WordDeal wd2 = new WordDeal(text2);
		WordDeal wd3 = new WordDeal(text3);
		WordDeal wd4 = new WordDeal(text4);

		int cn1 = wd1.getCharCount();
		int cn2 = wd2.getCharCount();
		int cn3 = wd3.getCharCount();
		int cn4 = wd4.getCharCount();
		
		assertEquals(0, cn1);
		assertEquals(0, cn2);
		assertEquals(80, cn3);
		assertEquals(73, cn4);
	
	}
	
	@Test
	public void testGetWordCount() throws IOException {//统计单词数量测试
		FileDeal fd = new FileDeal();
		String text1 = fd.FileToString("text/text1.txt");
		String text2 = fd.FileToString("text/text4.txt");
		
		WordDeal wd1 = new WordDeal(text1);
		WordDeal wd2 = new WordDeal(text2);
		
		int wn1 = wd1.getWordCount();
		int wn2 = wd2.getWordCount();
		
		assertEquals(0, wn1);
		assertEquals(3, wn2);
		
		
		
	}

	@Test
	public void testGetWordFreq() throws IOException {//统计词频测试
		Map<String,Integer> ans1 = new HashMap<String,Integer>();
		Map<String,Integer> ans2 = new HashMap<String,Integer>();
	
		ans2.put("coverage", 1);
		ans2.put("yizhishuijiao", 2);
		
		FileDeal fd = new FileDeal();
		String text1 = fd.FileToString("text/text1.txt");
		String text2 = fd.FileToString("text/text4.txt");
		
		WordDeal wd1 = new WordDeal(text1);
		WordDeal wd2 = new WordDeal(text2);
		
		Map<String,Integer> wf1 = wd1.getWordFreq();
		Map<String,Integer> wf2 = wd2.getWordFreq();
		
		assertEquals(ans1, wf1);
		assertEquals(ans2, wf2);
	}

	@Test
	public void testSortMap() throws IOException {//词频排序测试
		
		
		FileDeal fd = new FileDeal();
		String text1 = fd.FileToString("text/text1.txt");
		String text2 = fd.FileToString("text/text4.txt");
		
		WordDeal wd1 = new WordDeal(text1);
		WordDeal wd2 = new WordDeal(text2);
		
		Map<String,Integer> wf1 = wd1.getWordFreq();
		Map<String,Integer> wf2 = wd2.getWordFreq();
		
		List lis1 = wd1.sortMap(wf1);
		List lis2 = wd2.sortMap(wf2);
		
		//assertEquals(ans1, wf1);
		//assertEquals(ans2, wf2);
	}

	@Test
	public void testGetLineCount() throws IOException {//统计有效行数测试
		FileDeal fd = new FileDeal();
		String text1 = fd.FileToString("text/text1.txt");
		String text2 = fd.FileToString("text/text4.txt");
		
		WordDeal wd1 = new WordDeal(text1);
		WordDeal wd2 = new WordDeal(text2);
		
		int wn1 = wd1.getLineCount();
		int wn2 = wd2.getLineCount();
		
		assertEquals(0, wn1);
		assertEquals(4, wn2);
		
	
	}

	@Test
	public void testListToArray() throws IOException {
		
		
		FileDeal fd = new FileDeal();
		String text1 = fd.FileToString("text/text1.txt");
		String text2 = fd.FileToString("text/text4.txt");
		String text3 = fd.FileToString("text/text5.txt");
		
		WordDeal wd1 = new WordDeal(text1);
		WordDeal wd2 = new WordDeal(text2);
		WordDeal wd3 = new WordDeal(text3);
		
		Map<String,Integer> wf1 = wd1.getWordFreq();
		Map<String,Integer> wf2 = wd2.getWordFreq();
		Map<String,Integer> wf3 = wd3.getWordFreq();
		
		List lis1 = wd1.sortMap(wf1);
		List lis2 = wd2.sortMap(wf2);
		List lis3 = wd3.sortMap(wf3);
		
		String[] s1 = wd1.ListToArray(lis1);
		String[] s2 = wd2.ListToArray(lis2);
		String[] s3 = wd3.ListToArray(lis3);
	}

}
