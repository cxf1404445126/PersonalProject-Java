package junitTest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.*;

import org.junit.Before;
import org.junit.Test;

import wCount.FileDeal;
import wCount.WordDeal;

public class Perf_analy {

	FileDeal fd;
	String text1;
	WordDeal wd1;
	Map<String, Integer> wf1;
	List lis1;

	@Before
	public void setUp() throws Exception { // 初始化数据
		fd = new FileDeal();
		text1 = fd.FileToString("LittlePrince.txt");
		wd1 = new WordDeal(text1);
		wf1 = wd1.getWordFreq();
		lis1 = wd1.sortMap(wf1);

	}

	@Test
	public void testGetCharCount() throws IOException { // 测试统计字符数量函数

		long startTime = System.currentTimeMillis();
		wd1.getCharCount();
		long endTime = System.currentTimeMillis();
		System.out.println("getCharCount函数的运行时间为" + (endTime - startTime) + "毫秒");

	}

	@Test
	public void testGetWordCount() throws IOException { // 测试统计单词数量函数
		long startTime = System.currentTimeMillis();
		wd1.getWordCount();
		long endTime = System.currentTimeMillis();
		System.out.println("getWordCount函数的运行时间为：" + (endTime - startTime) + "毫秒");

	}

	@Test
	public void testGetWordFreq() throws IOException { // 测试统计词频函数
		long startTime = System.currentTimeMillis();
		wd1.getWordFreq();
		long endTime = System.currentTimeMillis();
		System.out.println("getWordFreq函数的运行时间为：" + (endTime - startTime) + "毫秒");

	}

	@Test
	public void testSortMap() throws IOException { // 测试词频排序函数
		long startTime = System.currentTimeMillis();
		wd1.sortMap(wf1);
		long endTime = System.currentTimeMillis();
		System.out.println("sortMap函数的运行时间为：" + (endTime - startTime) + "毫秒");

	}

	@Test
	public void testGetLineCount() throws IOException {// 测试统计有效行数函数
		long startTime = System.currentTimeMillis();
		wd1.getLineCount();
		long endTime = System.currentTimeMillis();
		System.out.println("getLineCount函数的运行时间为：" + (endTime - startTime) + "毫秒");

	}

}
